from collections import namedtuple
from .base import BaseWindowActionHandler
from .base import BaseInputActionHandler

import win32gui, win32con
from pynput import mouse, keyboard

"""
Action handler for windows system
"""  
class WindowActionHandler(BaseWindowActionHandler):
    @classmethod
    def GetHandler(cls):
        return cls()

    @staticmethod
    def GetActiveWindow():
        # returns the handle of the currently active window
        win_id = win32gui.GetForegroundWindow()
        return win_id

    @staticmethod
    def GetwindowLocation(win_id):
        # returns active window loaction -> {"x": start_x, "y":start_y}
        start_x, start_y, end_x, end_y = win32gui.GetWindowRect(win_id)
        location = namedtuple("location", ["x", "y"])
        return location(start_x, start_y)

    @staticmethod
    def GetWindowGeometry(win_id):
        # returns active window geometry -> {"width": width, "height": height}
        start_x, start_y, end_x, end_y = win32gui.GetWindowRect(win_id)
        width = end_x - start_x
        height = end_y - start_y
        geometry = namedtuple("geometry", ["width", "height"])
        # return {"width": width, "height": height}
        return geometry(width, height)

    @staticmethod
    def SelectWindow():
        # returns the window handle of the window selected by user
        x, y = win32gui.GetCursorPos()
        win_id = win32gui.WindowFromPoint((x, y))
        return win_id

    @staticmethod
    def WindowMove(win_id,win_posx:int, win_posy:int):
        # relocates active window to specified loaction
        current_size = WindowActionHandler.GetWindowGeometry(win_id)
        win32gui.SetWindowPos(win_id, win32con.HWND_TOP, win_posx, win_posy, current_size.width, current_size.height, win32con.SWP_SHOWWINDOW)

    @staticmethod
    def ResizeWindow(win_id,n_height:int,n_width:int):
        # resizes active window to specified dimensions
        current_position = WindowActionHandler.GetwindowLocation(win_id)        
        win32gui.SetWindowPos(win_id, win32con.HWND_TOP, current_position.x, current_position.y, n_width, n_height, win32con.SWP_SHOWWINDOW)

    @staticmethod
    def WindowFocus(win_id):
        # brings the window to top(focus) 
        win32gui.SetActiveWindow(win_id)
        # win32gui.SetForegroundWindow(win_id)

class InputActionHandler(BaseInputActionHandler):
    @classmethod
    def GetHandler(cls):
        return cls()
    
    def __init__(self) -> None:
        super().__init__()  
        self.mouse = mouse.Controller()
        self.mouse_listener = mouse.Listener
        self.mouse_buttons = mouse.Button
   
        self.keyboard = keyboard.Controller()
        self.keyboard_listener = keyboard.Listener
        self.keyboard_keys = keyboard.Key